package com.jdelgadom.springdataexamples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringdataexamplesApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringdataexamplesApplication.class, args);
  }
}
