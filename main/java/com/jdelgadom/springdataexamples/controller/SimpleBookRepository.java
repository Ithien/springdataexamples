package com.jdelgadom.springdataexamples.controller;

import com.jdelgadom.springdataexamples.model.Book;
import org.springframework.stereotype.Component;

@Component
public class SimpleBookRepository {

  public Book getByIsbn(String isbn) {
    simulateSlowService();
    return new Book(isbn, "Some book");
  }

  private void simulateSlowService() {
    try {
      long time = 3000L;
      Thread.sleep(time);
    } catch (InterruptedException e) {
      throw new IllegalStateException(e);
    }
  }
}
