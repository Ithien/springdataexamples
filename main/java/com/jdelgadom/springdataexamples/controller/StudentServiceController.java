package com.jdelgadom.springdataexamples.controller;

import com.jdelgadom.springdataexamples.model.Student;
import com.jdelgadom.springdataexamples.repository.StudentRepository;
import com.jdelgadom.springdataexamples.repository.custom.StudentCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @NameClass: StudentServiceController
 * @author: @jdelgadomolto
 * @Description: Create a Java microservice for the StudentService
 * @DateCreation: 16/11/2018
 */
@RestController
public class StudentServiceController {

  @Autowired
  StudentRepository studentRepository;

  /**
   * @Name: getStudents
   * @Description: getmapping to obtain the list of all the students
   * @return list all students
   */
  @GetMapping("/listStudents")
  public List<Student> getStudents(){
    //1- init database H2 embebed
    List<Student> listStudent = initStudents();
    return listStudent;
  }

  //TODO: This mapping test the jdbcTemplate and obtain for other Century xD
  @GetMapping("/listStudentsDeprecated")
  public List<Student> getStudentsDeprecated(){
    List<Student> listStudent = initStudents();
    StudentCustom studentCustom = new StudentCustom();
    //TODO: Rellenar el data Source studentCustom.setDataSource();
    return studentCustom.listStudents();
  }

  /**
   * @Name: initStudents
   * @Description: Init the Database table Students to test the microservices
   * @return list of init students
   */
  private List<Student> initStudents() {
    List<Student> listStudent = new ArrayList<>();
    Student student = new Student();
    student.setName("Jose");
    student.setAge(33L);
    listStudent.add(student);
    studentRepository.save(student);
    student = new Student();
    student.setName("Salvatore");
    student.setAge(40L);
    studentRepository.save(student);
    listStudent.add(student);
    return listStudent;
  }
}
