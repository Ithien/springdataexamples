package com.jdelgadom.springdataexamples.controller;

import com.jdelgadom.springdataexamples.model.Product;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductServiceController{

  @GetMapping(value = "/listProduct")
  public ResponseEntity<Object> getProducts(){
    return new ResponseEntity<>(productRepo.values(), HttpStatus.OK);
  }

  @PostMapping(value = "/createProduct")
  public ResponseEntity<Object> createProduct(@RequestBody Product product){
    //1. Se realiza el create del producto
    productRepo.put(product.getId(),product);
    return new ResponseEntity<>("Se ha creado un nuevo registro", HttpStatus.CREATED);
  }

  @RequestMapping(value = "/products/{id}",method = RequestMethod.DELETE)
  public ResponseEntity<Object> deleteProduct(@PathVariable("id") String id){
    productRepo.remove(id);
    return new ResponseEntity<>("Se ha borrado el producto correctamente", HttpStatus.OK);
  }

  @RequestMapping(value = "/products/{id}",method = RequestMethod.PUT)
  public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Product product){
    productRepo.remove(id);
    product.setId(id);
    productRepo.put(product.getId(),product);
    return new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK);
  }


  private static Map<String, Product> productRepo = new HashMap<>();
  static {
    Product honey = new Product();
    honey.setId("1");
    honey.setName("Honey");
    productRepo.put(honey.getId(), honey);

    Product almond = new Product();
    almond.setId("2");
    almond.setName("Almond");
    productRepo.put(almond.getId(), almond);
  }
  
}
