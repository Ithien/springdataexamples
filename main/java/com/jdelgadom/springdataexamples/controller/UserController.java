package com.jdelgadom.springdataexamples.controller;


import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @NameClass: UserController
 * @author: @jdelgadomolto
 * @Description: Create a Java microservice for the UserController to test Oauth2 example
 * @DateCreation: 16/11/2018
 */
@RestController
public class UserController {

  /**
   * @Name: user
   * @Description: getmapping to attributes of the authentication token.
   *               see more info in: "CommonOAuth2Provider.java", "OAuth2AuthorizationRequestRedirectFilter"
   *               "OAuth2LoginAuthenticationFilter", "OAuth2LoginAuthenticationProvider"
   *               idp - Our identity Provider - Config app uri
   * @return Map of attributes of the authentication token.
   */
   @GetMapping(value = "/user")
   Map<String, Object> user(OAuth2AuthenticationToken authenticationToken){
    return authenticationToken.getPrincipal().getAttributes();
  }
}
