package com.jdelgadom.springdataexamples.repository;

import com.jdelgadom.springdataexamples.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @NameClass: StudentRepository
 * @author: @jdelgadomolto
 * @Description: StudentRepository extends to JpaRepository and use SpringData from DAO - Data Access Object
 * @DateCreation: 16/11/2018
 */
@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
}
