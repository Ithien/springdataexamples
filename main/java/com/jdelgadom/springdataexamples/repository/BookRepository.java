package com.jdelgadom.springdataexamples.repository;

import com.jdelgadom.springdataexamples.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @NameClass: BookRepository
 * @author: @jdelgadomolto
 * @Description: BookRepository extends to JpaRepository and use SpringData from DAO - Data Access Object
 * @DateCreation: 16/11/2018
 */
public interface BookRepository extends JpaRepository<Book,Long> {

  Book getByIsbn(String isbn);

}
