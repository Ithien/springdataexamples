package com.jdelgadom.springdataexamples.repository.custom;

import com.jdelgadom.springdataexamples.model.Student;
import com.jdelgadom.springdataexamples.model.StudentMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * @NameClass: StudentCustom
 * @author: @jdelgadomolto
 * @Description: StudentCustom class to test the diferents connection of the Docker Configuration: MongoDB, MariaDb, Reddis
 * @DateCreation: 16/11/2018
 * TODO: This class is for implement in a future
 */
public class StudentCustom {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
    this.jdbcCall =  new SimpleJdbcCall(dataSource).withProcedureName("getRecord");
  }

  public Student getStudent(Long id) {
    SqlParameterSource in = new MapSqlParameterSource().addValue("in_id", id);
    Map<String, Object> out = jdbcCall.execute(in);

    Student student = new Student();
    student.setId(id);
    student.setName((String) out.get("out_name"));
    student.setAge((Long) out.get("out_age"));
    return student;

  }

  public List<Student> listStudents() {

    JdbcTemplate jdbcTemplateObject = new JdbcTemplate((javax.sql.DataSource) dataSource);
    String SQL = "select * from Student";
    List <Student> students = jdbcTemplateObject.query(SQL, new StudentMapper());
    return students;
  }


}
