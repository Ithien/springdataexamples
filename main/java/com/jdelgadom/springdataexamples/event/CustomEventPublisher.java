package com.jdelgadom.springdataexamples.event;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

/**
 * @NameClass: CustomEventPublisher
 * @author: @jdelgadomolto
 * @Description: this is object to publish the custom event, then other app can subscribe for this publish
 * @DateCreation: 16/11/2018
 */
public class CustomEventPublisher implements ApplicationEventPublisherAware {

  private ApplicationEventPublisher publisher;

  public void publish() {
    CustomEvent ce = new CustomEvent(this);
    publisher.publishEvent(ce);
  }

  @Override
  public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) { ;
    this.publisher = applicationEventPublisher;
  }
}
