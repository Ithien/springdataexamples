package com.jdelgadom.springdataexamples.event;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @NameClass: MainAppEvent
 * @author: @jdelgadomolto
 * @Description: Main to test the config of a customEvent
 * @DateCreation: 16/11/2018
 */
public class MainAppEvent {
  public static void main(String[] args) {
    //1. Load the AplicationContext to this test example
    ConfigurableApplicationContext context =   new ClassPathXmlApplicationContext("Beans.xml");
    //2. Load from the AplicationContext the bean publisher
    CustomEventPublisher cvp =  (CustomEventPublisher) context.getBean("customEventPublisher");
    //3. Read the publish from the publisher
    cvp.publish();
    cvp.publish();
  }
}
