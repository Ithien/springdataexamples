package com.jdelgadom.springdataexamples.event;

import org.springframework.context.ApplicationEvent;

/**
 * @NameClass: CustomEvent
 * @author: @jdelgadomolto
 * @Description: this is object to create customize event example:
 *               CustomEvent extends to ApplicationEvent
 * @DateCreation: 16/11/2018
 */
public class CustomEvent extends ApplicationEvent {
  public CustomEvent(Object source) {
    super(source);
  }

  public String toString(){
    return "my custom event";
  }
}
