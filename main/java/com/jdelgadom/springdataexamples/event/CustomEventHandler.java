package com.jdelgadom.springdataexamples.event;

import org.springframework.context.ApplicationListener;

/**
 * @NameClass: CustomEventHandler
 * @author: @jdelgadomolto
 * @Description: this is object to create customize event example:
 *               implements the onApplicationEvent to test the customEvent
 * @DateCreation: 16/11/2018
 */
public class CustomEventHandler implements ApplicationListener<CustomEvent> {
  @Override
  public void onApplicationEvent(CustomEvent customEvent) {
    System.out.println(customEvent.toString());
  }
}
