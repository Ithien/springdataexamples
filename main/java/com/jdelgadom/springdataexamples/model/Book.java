package com.jdelgadom.springdataexamples.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @NameClass: Student
 * @author: @jdelgadomolto
 * @Description: Entity Student to testing a RestController with this database object
 * @DateCreation: 16/11/2018
 */
@Entity
public class Book {
  @Id
  @Column(name = "ID")
  private String isbn;
  @Column(name = "title")
  private String title;

  public Book(String isbn, String title) {
    this.isbn = isbn;
    this.title = title;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String toString() {
    return "Book{" + "isbn='" + isbn + '\'' + ", title='" + title + '\'' + '}';
  }
}
