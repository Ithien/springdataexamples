package com.jdelgadom.springdataexamples.model;

import javax.persistence.*;

/**
 * @NameClass: Greeting
 * @author: @jdelgadomolto
 * @Description: Entity Greeting to testing a RestController with this database object
 * @DateCreation: 16/11/2018
 */
@Entity
public class Greeting {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private long id;
  @Column(name = "CONTENT")
  private String content;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
