package com.jdelgadom.springdataexamples.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @NameClass: Product
 * @author: @jdelgadomolto
 * @Description: Entity Product to testing a RestController with this database object
 * @DateCreation: 16/11/2018
 */
@Entity
public class Product {

  @Id
  @Column(name = "ID")
  private String id;
  @Column(name = "NAME")
  private String name;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
