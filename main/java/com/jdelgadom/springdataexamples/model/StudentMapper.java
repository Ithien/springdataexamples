package com.jdelgadom.springdataexamples.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @NameClass: StudentMapper
 * @author: @jdelgadomolto
 * @Description: Class to get a mapper with RowMapper and then parse the resultset to object Student a generic
 * @DateCreation: 16/11/2018
 */
public class StudentMapper implements RowMapper<Student> {
  @Override
  public Student mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    Student student = new Student();
    student.setId(resultSet.getLong("id"));
    student.setName(resultSet.getString("name"));
    student.setAge(resultSet.getLong("age"));

    return student;

  }
}
