package com.jdelgadom.springdataexamples.model;


import javax.persistence.*;

/**
 * @NameClass: Student
 * @author: @jdelgadomolto
 * @Description: Entity Student to testing a RestController with this database object
 * @DateCreation: 16/11/2018
 */
@Entity
public class Student {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID")
  private Long id;
  @Column(name="NAME",length = 20)
  private String name;
  @Column(name="AGE")
  private Long age;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getAge() {
    return age;
  }

  public void setAge(Long age) {
    this.age = age;
  }

}
