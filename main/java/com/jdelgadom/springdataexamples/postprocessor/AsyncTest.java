package com.jdelgadom.springdataexamples.postprocessor;

public class AsyncTest implements Runnable{

  public void asyncMethod() throws InterruptedException {
   // TimeUnit.SECONDS.sleep(5);
    System.out.println("POLLOVACA");
  }

  /**
   * When an object implementing interface <code>Runnable</code> is used
   * to create a thread, starting the thread causes the object's
   * <code>run</code> method to be called in that separately executing
   * thread.
   * <p>
   * The general contract of the method <code>run</code> is that it may
   * take any action whatsoever.
   *
   * @see Thread#run()
   */
  @Override
  public void run() {
    try {
      asyncMethod();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
