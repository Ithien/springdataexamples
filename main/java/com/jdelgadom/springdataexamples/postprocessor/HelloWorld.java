package com.jdelgadom.springdataexamples.postprocessor;

/**
 * @NameClass: HelloWorld
 * @author: @jdelgadomolto
 * @Description: HelloWorld Object to test the postProcessor Bean (BeanPostProcessor) of Spring
 * @DateCreation: 16/11/2018
 */
public class HelloWorld {

  private String message;

  public void getMessage() {
    System.out.println("Your Message : " + message);
  }

  public void init() {
    System.out.println("Bean is going through init.");
  }

  public void destroy() {
    System.out.println("Bean will destroy now.");
  }


  public void setMessage(String message) {
    this.message = message;
  }
}
