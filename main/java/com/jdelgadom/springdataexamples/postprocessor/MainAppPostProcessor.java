package com.jdelgadom.springdataexamples.postprocessor;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @NameClass: MainAppPostProcessor
 * @author: @jdelgadomolto
 * @Description: Main class to test the postProcessor Bean (BeanPostProcessor) of Spring
 *               This process need to execute something before the init Bean or Before the execute
 *               Bean and after init the bean
 * @DateCreation: 16/11/2018
 */
public class MainAppPostProcessor {
  public static void main(String[] args) {

    //1. Load the aplication context with the config Beans
    AbstractApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

    //2. Init HelloWorld Object from the config Beans.xml
    HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
    obj.getMessage();

    //3. Line to catch and test the BeanPostProcessor
    context.registerShutdownHook();
  }

}
