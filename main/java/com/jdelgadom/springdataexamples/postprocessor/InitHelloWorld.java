package com.jdelgadom.springdataexamples.postprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @NameClass: HelloWorld
 * @author: @jdelgadomolto
 * @Description: InitHelloWorld class to implements the BeanPostProcessor
 * @DateCreation: 16/11/2018
 */
public class InitHelloWorld implements BeanPostProcessor {

  /**
   * @Name: postProcessBeforeInitialization
   * @Description: function to test the  BeanPostProcessor: this function execute before the BeanPostProcessor
   * @param bean: Object Bean to use in the function
   * @param beanName: BeanName define in the Beans.xml or with de annotation @Bean
   * @return the bean object for the test
   */
  public Object postProcessBeforeInitialization(Object bean, String beanName)
     throws BeansException {
    System.out.println("BeforeInitialization : " + beanName);
    return bean;  // you can return any other object as well
  }

  /**
   * @Name: postProcessAfterInitialization
   * @Description: function to test the  BeanPostProcessor: this function execute after the BeanPostProcessor
   * @param bean: Object Bean to use in the function
   * @param beanName: BeanName define in the Beans.xml or with de annotation @Bean
   * @return the bean object for the test
   */
  public Object postProcessAfterInitialization(Object bean, String beanName)
     throws BeansException {
    System.out.println("AfterInitialization : " + beanName);
    return bean;  // you can return any other object as well
  }

}
