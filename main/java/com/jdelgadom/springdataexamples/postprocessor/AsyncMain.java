package com.jdelgadom.springdataexamples.postprocessor;


import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.CompletableFuture;

@EnableAsync
public class AsyncMain {

  public static void main (String [] args) throws InterruptedException {
    System.out.println("hola holita");
    AsyncTest asyncTest =  new AsyncTest();
    //asyncTest.asyncMethod();
    CompletableFuture.runAsync(asyncTest);
    System.out.println("vecinito");
  }
}
