package com.jdelgadom.springdataexamples;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * Created by @jdelgadomolto on 14/05/2018.
 */
//Se activa Swagger para hacer pruebas de web.
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  //Se arranca el API de swagger para poder probar.
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
      .select()
      .apis(RequestHandlerSelectors.any())
      .paths(PathSelectors.any())
      .build()
      .apiInfo(apiInfo());
  }

  //Customización de la API
  //Se puedo ir modificando la información y los estilos mostrados en la web.
  //Con esto configuramos el Api para realizar pruebas desde un UX generado
  private ApiInfo apiInfo() {
    return new ApiInfo(
      "JOSE PROYECT",
      "Api de REST Controller",
      "version 1.0.0",
      "Terms of service",
      new Contact("jose", "", "demojo08@gva.es"),
      "License of API", "API license URL", Collections.emptyList());
  }


}