package com.jdelgadom.springdataexamples.designpatterns.builder.components;

import com.jdelgadom.springdataexamples.designpatterns.builder.car.Car;

/**
 *
 * @NameClass: Engine
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example builder
 *               for page: https://refactoring.guru/design-patterns/builder/java/example
 *               This is to learn Design of patterns: Builder
 *               Class contains the TripComputer of the car, witch is a component
 * @DateCreation: 28/12/2018
 *
 */
public class TripComputer {

  private Car car;

  public void setCar(Car car) {
    this.car = car;
  }

  public void showFuelLevel() {
    System.out.println("Fuel level: " + car.getFuel());
  }

  public void showStatus() {
    if (this.car.getEngine().isStarted()) {
      System.out.println("Car is started");
    } else {
      System.out.println("Car isn't started");
    }
  }

}
