package com.jdelgadom.springdataexamples.designpatterns.builder.components;

/**
 * Enum to test the design pattern Builder. type of Transmission for a Car
 */
public enum Transmission {
  SINGLE_SPEED, MANUAL, AUTOMATIC, SEMI_AUTOMATIC
}
