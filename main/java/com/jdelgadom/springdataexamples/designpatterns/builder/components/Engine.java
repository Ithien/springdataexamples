package com.jdelgadom.springdataexamples.designpatterns.builder.components;

/**
 *
 * @NameClass: Engine
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example builder
 *               for page: https://refactoring.guru/design-patterns/builder/java/example
 *               This is to learn Design of patterns: Builder
 *               Class contains the Engine of the car, witch is a component
 * @DateCreation: 28/12/2018
 *
 */
public class Engine {
  private final double volume;
  private double mileage;
  private boolean started;

  public Engine(double volume, double mileage) {
    this.volume = volume;
    this.mileage = mileage;
  }

  public void on() {
    started = true;
  }

  public void off() {
    started = false;
  }

  public boolean isStarted() {
    return started;
  }

  public void go(double mileage) {
    if (started) {
      this.mileage += mileage;
    } else {
      System.err.println("Cannot go(), you must start engine first!");
    }
  }

  public double getVolume() {
    return volume;
  }

  public double getMileage() {
    return mileage;
  }
}
