package com.jdelgadom.springdataexamples.designpatterns.builder.components;

/**
 *
 * @NameClass: GPSNavigator
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example builder
 *               for page: https://refactoring.guru/design-patterns/builder/java/example
 *               This is to learn Design of patterns: Builder
 *               Class contains the GPSNavigator of the car, witch is a component
 * @DateCreation: 28/12/2018
 *
 */
public class GPSNavigator {
  private String route;

  public GPSNavigator() {
    this.route = "221b, Baker Street, London  to Scotland Yard, 8-10 Broadway, London";
  }

  public GPSNavigator(String manualRoute) {
    this.route = manualRoute;
  }

  public String getRoute() {
    return route;
  }
}
