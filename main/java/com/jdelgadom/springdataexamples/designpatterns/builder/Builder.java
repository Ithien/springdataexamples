package com.jdelgadom.springdataexamples.designpatterns.builder;

import com.jdelgadom.springdataexamples.designpatterns.builder.car.Type;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.Engine;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.GPSNavigator;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.Transmission;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.TripComputer;

/**
 *
 * @NameClass: Builder
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example builder
 *               for page: https://refactoring.guru/design-patterns/builder/java/example
 *               This is to learn Design of patterns: Builder
 *               Builder interface defines all possible ways to configure a product.
 *               We have two products Car, Manual. Product are the same, with other properties or definitions
 * @DateCreation: 28/12/2018
 *
 */
public interface Builder {
  void setType(Type type);
  void setSeats(int seats);
  void setEngine(Engine engine);
  void setTransmission(Transmission transmission);
  void setTripComputer(TripComputer tripComputer);
  void setGPSNavigator(GPSNavigator gpsNavigator);
}
