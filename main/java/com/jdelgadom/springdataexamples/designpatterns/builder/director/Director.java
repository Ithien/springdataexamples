package com.jdelgadom.springdataexamples.designpatterns.builder.director;

import com.jdelgadom.springdataexamples.designpatterns.builder.Builder;
import com.jdelgadom.springdataexamples.designpatterns.builder.car.Type;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.Engine;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.GPSNavigator;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.Transmission;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.TripComputer;


/**
 *
 * @NameClass: Director
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example builder
 *               for page: https://refactoring.guru/design-patterns/builder/java/example
 *               This is to learn Design of patterns: Builder
 *               Director of the Builder Pattern: Director defines the order of building steps.
 *               It works with a builder object through common Builder interface
 *               Therefore it may not know what product is being built
 * @DateCreation: 31/12/2018
 *
 */
public class Director {

  public void constructSportsCar(Builder builder) {
    builder.setType(Type.SPORTS_CAR);
    builder.setSeats(2);
    builder.setEngine(new Engine(3.0, 0));
    builder.setTransmission(Transmission.SEMI_AUTOMATIC);
    builder.setTripComputer(new TripComputer());
    builder.setGPSNavigator(new GPSNavigator());
  }

  public void constructCityCar(Builder builder) {
    builder.setType(Type.CITY_CAR);
    builder.setSeats(2);
    builder.setEngine(new Engine(1.2, 0));
    builder.setTransmission(Transmission.AUTOMATIC);
    builder.setTripComputer(new TripComputer());
    builder.setGPSNavigator(new GPSNavigator());
  }

  public void constructSUV(Builder builder) {
    builder.setType(Type.SUV);
    builder.setSeats(4);
    builder.setEngine(new Engine(2.5, 0));
    builder.setTransmission(Transmission.MANUAL);
    builder.setGPSNavigator(new GPSNavigator());
  }
}
