package com.jdelgadom.springdataexamples.designpatterns.builder;

import com.jdelgadom.springdataexamples.designpatterns.builder.car.Car;
import com.jdelgadom.springdataexamples.designpatterns.builder.car.Type;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.Engine;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.GPSNavigator;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.Transmission;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.TripComputer;

/**
 * @NameClass: Builder
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example builder
 * for page: https://refactoring.guru/design-patterns/builder/java/example
 * This is to learn Design of patterns: Builder
 * Concrete builders implement steps defined in the common interface.
 * @DateCreation: 28/12/2018
 */
public class CarBuilder implements Builder {
  private Type type;
  private int seats;
  private Engine engine;
  private Transmission transmission;
  private TripComputer tripComputer;
  private GPSNavigator gpsNavigator;

  @Override
  public void setType(Type type) {
    this.type = type;
  }

  @Override
  public void setSeats(int seats) {
    this.seats = seats;
  }

  @Override
  public void setEngine(Engine engine) {
    this.engine = engine;
  }

  @Override
  public void setTransmission(Transmission transmission) {
    this.transmission = transmission;
  }

  @Override
  public void setTripComputer(TripComputer tripComputer) {
    this.tripComputer = tripComputer;
  }

  @Override
  public void setGPSNavigator(GPSNavigator gpsNavigator) {
    this.gpsNavigator = gpsNavigator;
  }

  public Car getResult() {
    return new Car(type, seats, engine, transmission, tripComputer, gpsNavigator);
  }
}
