package com.jdelgadom.springdataexamples.designpatterns.builder.car;

import com.jdelgadom.springdataexamples.designpatterns.builder.components.Engine;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.GPSNavigator;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.Transmission;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.TripComputer;

/**
 * @NameClass: Manual
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example builder
 * for page: https://refactoring.guru/design-patterns/builder/java/example
 * This is to learn Design of patterns: Builder
 * Manual is a Product Class
 * @DateCreation: 31/12/2018
 */
public class Manual {
  private final Type type;
  private final int seats;
  private final Engine engine;
  private final Transmission transmission;
  private final TripComputer tripComputer;
  private final GPSNavigator gpsNavigator;

  public Manual(Type type, int seats, Engine engine, Transmission transmission,
                TripComputer tripComputer, GPSNavigator gpsNavigator) {
    this.type = type;
    this.seats = seats;
    this.engine = engine;
    this.transmission = transmission;
    this.tripComputer = tripComputer;
    this.gpsNavigator = gpsNavigator;
  }

  public String print() {
    String info = "";
    info += "TypeVehicles of car: " + type + "\n";
    info += "Count of seats: " + seats + "\n";
    info += "Engine: volume - " + engine.getVolume() + "; mileage - " + engine.getMileage() + "\n";
    if (this.tripComputer != null) {
      info += "Trip VehicleOperations: Functional" + "\n";
    } else {
      info += "Trip VehicleOperations: N/A" + "\n";
    }
    if (this.gpsNavigator != null) {
      info += "GPS Navigator: Functional" + "\n";
    } else {
      info += "GPS Navigator: N/A" + "\n";
    }
    return info;
  }
}
