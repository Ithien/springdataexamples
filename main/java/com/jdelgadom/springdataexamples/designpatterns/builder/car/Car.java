package com.jdelgadom.springdataexamples.designpatterns.builder.car;

import com.jdelgadom.springdataexamples.designpatterns.builder.components.Engine;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.GPSNavigator;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.Transmission;
import com.jdelgadom.springdataexamples.designpatterns.builder.components.TripComputer;

/**
 *
 * @NameClass: Car
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example builder
 *               for page: https://refactoring.guru/design-patterns/builder/java/example
 *               This is to learn Design of patterns: Builder
 *               Car is a Product Class
 * @DateCreation: 31/12/2018
 *
 */
public class Car {
  private final Type type;
  private final int seats;
  private final Engine engine;
  private final Transmission transmission;
  private final TripComputer tripComputer;
  private final GPSNavigator gpsNavigator;
  private double fuel = 0;

  public Car(Type type, int seats, Engine engine, Transmission transmission,
             TripComputer tripComputer, GPSNavigator gpsNavigator) {
    this.type = type;
    this.seats = seats;
    this.engine = engine;
    this.transmission = transmission;
    this.tripComputer = tripComputer;
    this.tripComputer.setCar(this);
    this.gpsNavigator = gpsNavigator;
  }

  public Type getType() {
    return type;
  }

  public double getFuel() {
    return fuel;
  }

  public void setFuel(double fuel) {
    this.fuel = fuel;
  }

  public int getSeats() {
    return seats;
  }

  public Engine getEngine() {
    return engine;
  }

  public Transmission getTransmission() {
    return transmission;
  }

  public TripComputer getTripComputer() {
    return tripComputer;
  }

  public GPSNavigator getGpsNavigator() {
    return gpsNavigator;
  }

}
