package com.jdelgadom.springdataexamples.designpatterns.builder.car;

/**
 * Enum to test the design pattern Builder. type of cars
 */
public enum Type {
  CITY_CAR, SPORTS_CAR, SUV
}
