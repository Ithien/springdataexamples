package com.jdelgadom.springdataexamples.designpatterns;

import com.jdelgadom.springdataexamples.designpatterns.abstractfactory.Iphone;
import com.jdelgadom.springdataexamples.designpatterns.abstractfactory.Phone;
import com.jdelgadom.springdataexamples.designpatterns.abstractfactory.TypePhone;
import com.jdelgadom.springdataexamples.designpatterns.abstractfactory.XiaomiPhone;

public class PhoneFactory {

  public static Phone getPhone(String type) {
    if (TypePhone.XIAOMI.getName().equalsIgnoreCase(type)) {
      return new XiaomiPhone();
    } else if (TypePhone.IPHONE.getName().equalsIgnoreCase(type)) {
      return new Iphone();
    } else {
      return null;
    }
  }

}
