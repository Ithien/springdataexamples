package com.jdelgadom.springdataexamples.designpatterns.factory;

/**
 * @NameClass: VehicleOperationsImpl
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example factory
 * This is to learn Design of patterns: Factory
 * To test the Factory Design, generate a Enum with 2 types of vehicles
 * @DateCreation: 02/01/2019
 */
public enum TypeVehicles {
  CAR("C"),MOTOBIKE("M");

  private String name;

  TypeVehicles(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
