package com.jdelgadom.springdataexamples.designpatterns.factory;

/**
 * @NameClass: VehicleOperations
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example factory
 * This is to learn Design of patterns: Factory
 * Interface define the Vehicle operations methods
 * @DateCreation: 28/12/2018
 */
public interface VehicleOperations {
  String on();
  String off();
  String getSound();
}
