package com.jdelgadom.springdataexamples.designpatterns.factory;

/**
 * @NameClass: MotobikeOperations
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example factory
 * This is to learn Design of patterns: Factory
 * To test the Factory Design, specific class to implements specific method for each Vehicle
 * @DateCreation: 02/01/2019
 */
public class MotobikeOperations extends VehicleOperationsImpl {

  public static final String SOUND_OF_MOTO = "SOUND OF MOTO brim brim";

  @Override
  public String getSound() {
    return SOUND_OF_MOTO;
  }
}
