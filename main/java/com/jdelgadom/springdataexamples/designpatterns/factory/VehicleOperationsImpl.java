package com.jdelgadom.springdataexamples.designpatterns.factory;

/**
 * @NameClass: VehicleOperationsImpl
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example factory
 * This is to learn Design of patterns: Factory
 * Implement the common operations of the vehicles in the abstract class
 * @DateCreation: 02/01/2019
 */
public abstract class VehicleOperationsImpl implements VehicleOperations {

  public String on(){
    return "He encendido el vehiculo";
  }

  public String off(){
    return "He apagado el vehiculo";
  }

}
