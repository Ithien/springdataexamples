package com.jdelgadom.springdataexamples.designpatterns.factory;

/**
 * @NameClass: VehicleOperationsFactory
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example factory
 * This is to learn Design of patterns: Factory
 * Class to delegate the creation object for the vehicles operations
 * @DateCreation: 02/01/2019
 */
public class VehicleOperationsFactory {

  public static VehicleOperations getVehicleOperations(String type){
    if(TypeVehicles.CAR.getName().equalsIgnoreCase(type)){
      return new CarOperations();
    } else if(TypeVehicles.MOTOBIKE.getName().equalsIgnoreCase(type)) {
      return new MotobikeOperations();
    } else {
      return null;
    }
  }

}
