package com.jdelgadom.springdataexamples.designpatterns.factory;

/**
 * @NameClass: DemoFactory
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example factory
 * This is to learn Design of patterns: Factory
 * Demo to test the Design Factory implementation
 * @DateCreation: 02/01/2019
 */
public class DemoFactory {

  public static void main(String[] args) {
    VehicleOperations car = VehicleOperationsFactory.getVehicleOperations(TypeVehicles.CAR.getName());
    VehicleOperations moto = VehicleOperationsFactory.getVehicleOperations(TypeVehicles.MOTOBIKE.getName());
    System.out.println("Factory Car Sound: " + car.getSound());
    System.out.println("Factory Moto Sound: " + moto.getSound());
  }

}
