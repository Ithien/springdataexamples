package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

/**
 * @NameClass: Phone
 * @author: @jdelgadomolto
 * @Description: This code are examples for design of patterns example factory
 * This is to learn Design of patterns: AbstractFactory
 * Interface define the Phone methods
 * @DateCreation: 03/01/2018
 */
public interface Phone {
  String on();
  String off();
  String getSound();
}
