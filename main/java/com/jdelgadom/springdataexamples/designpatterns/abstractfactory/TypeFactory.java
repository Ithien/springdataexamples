package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

public enum TypeFactory {
  VEHICLE("VEHICLE"),PHONE("PHONE");

  private String name;

  TypeFactory(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
