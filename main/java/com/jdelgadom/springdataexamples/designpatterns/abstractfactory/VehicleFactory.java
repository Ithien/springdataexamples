package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

import com.jdelgadom.springdataexamples.designpatterns.factory.CarOperations;
import com.jdelgadom.springdataexamples.designpatterns.factory.MotobikeOperations;
import com.jdelgadom.springdataexamples.designpatterns.factory.TypeVehicles;
import com.jdelgadom.springdataexamples.designpatterns.factory.VehicleOperations;

public class VehicleFactory extends AbstractFactory {

  @Override
  VehicleOperations getSoundVehicle(String Vehicle) {
    if(TypeVehicles.CAR.getName().equalsIgnoreCase(Vehicle)){
      return new CarOperations();
    } else if(TypeVehicles.MOTOBIKE.getName().equalsIgnoreCase(Vehicle)) {
      return new MotobikeOperations();
    } else {
      throw new RuntimeException("Runtime");
    }
  }

  @Override
  Phone getSoundPhone(String Phone) {
    return null;
  }
}
