package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

public class Iphone extends PhoneImpl {
  @Override
  public String getSound() {
    return "Iphone SOUNDS BIP BIP";
  }
}
