package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

public class FactoryProducer {
  public static AbstractFactory getFactory(String factoryChoice) {

    if (TypeFactory.VEHICLE.getName().equalsIgnoreCase(factoryChoice)) {
      return new VehicleFactory();
    } else if (TypeFactory.PHONE.getName().equalsIgnoreCase(factoryChoice)) {
      return new PhoneFactory();
    } else {
      throw new RuntimeException("Runtime");
    }
  }
}
