package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

import com.jdelgadom.springdataexamples.designpatterns.factory.VehicleOperations;

public class PhoneFactory extends AbstractFactory{
  @Override
  VehicleOperations getSoundVehicle(String Vehicle) {
    return null;
  }

  @Override
  Phone getSoundPhone(String Phone) {

    if(TypePhone.XIAOMI.getName().equalsIgnoreCase(Phone)){
      return new XiaomiPhone();
    }else if(TypePhone.IPHONE.getName().equalsIgnoreCase(Phone)){
      return new Iphone();
    }else{
      throw new RuntimeException("Runtime");
    }
  }
}
