package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

abstract class PhoneImpl implements Phone{
  public String on(){
   return "turn On phone";
  };

  public String off(){
    return "turn Off phone";
  };
}
