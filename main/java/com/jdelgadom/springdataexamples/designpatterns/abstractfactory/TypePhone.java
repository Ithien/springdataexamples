package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

public enum TypePhone {
  XIAOMI("X"),IPHONE("A");

  private String name;

  TypePhone(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
