package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

public class XiaomiPhone extends PhoneImpl {
  @Override
  public String getSound() {
    return "XIAOMI SOUND PHONE BRUUUUUUU BRUUUUUU";
  }
}
