package com.jdelgadom.springdataexamples.designpatterns.abstractfactory;

import com.jdelgadom.springdataexamples.designpatterns.factory.VehicleOperations;

public abstract class AbstractFactory {
  abstract VehicleOperations getSoundVehicle(String Vehicle);
  abstract Phone getSoundPhone(String Phone);
}
