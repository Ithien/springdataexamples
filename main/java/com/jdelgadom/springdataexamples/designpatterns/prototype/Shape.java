package com.jdelgadom.springdataexamples.designpatterns.prototype;

import java.util.Objects;

/**
 * @NameClass: Shape
 * @author: @jdelgadomolto
 * @Description: This code are examples for dessign of patterns extract
 *               for page: https://refactoring.guru/design-patterns/prototype/java/example
 *               This is to learn Dessign of patterns: Protoryppe
 *               Class generic of Shape to extends the others class: Circle, Rectangle...
 * @DateCreation: 20/12/2018
 */
public abstract class Shape {

  public int x;
  public int y;
  public String color;

  public Shape() {
  }

  public Shape(Shape target) {
    if (target != null) {
      this.x = target.x;
      this.y = target.y;
      this.color = target.color;
    }
  }

  public abstract Shape clone();

  @Override
  public boolean equals(Object object2) {
    if (!(object2 instanceof Shape)) return false;
    Shape shape2 = (Shape) object2;
    return shape2.x == x && shape2.y == y && Objects.equals(shape2.color, color);
  }

}
