package com.jdelgadom.springdataexamples.designpatterns.prototype;

/**
 * @NameClass: Circle
 * @author: @jdelgadomolto
 * @Description: This code are examples for dessign of patterns extract
 *               for page: https://refactoring.guru/design-patterns/prototype/java/example
 *               This is to learn Dessign of patterns: Protoryppe
 *               Class extends for Shape and add a property rectangle,
 *               width, height and specific methods...
 * @DateCreation: 20/12/2018
 */
public class Rectangle extends Shape {

  public int width;
  public int height;

  public Rectangle() {
  }

  public Rectangle(Rectangle target) {
    super(target);
    if (target != null) {
      this.width = target.width;
      this.height = target.height;
    }
  }

  @Override
  public Shape clone() {
    return new Rectangle(this);
  }

  @Override
  public boolean equals(Object object2) {
    if (!(object2 instanceof Rectangle) || !super.equals(object2)) return false;
    Rectangle shape2 = (Rectangle) object2;
    return shape2.width == width && shape2.height == height;
  }

}
