package com.jdelgadom.springdataexamples.designpatterns.prototype;

/**
 * @NameClass: Circle
 * @author: @jdelgadomolto
 * @Description: This code are examples for dessign of patterns extract
 *               for page: https://refactoring.guru/design-patterns/prototype/java/example
 *               This is to learn Dessign of patterns: Protoryppe
 *               Class extends for Shape and add a property circle, radius and specific methods...
 * @DateCreation: 20/12/2018
 */
public class Circle extends Shape{

  public int radius;

  public Circle() {
  }

  public Circle(Circle target) {
    super(target);
    if (target != null) {
      this.radius = target.radius;
    }
  }

  @Override
  public Shape clone() {
    return new Circle(this);
  }

  @Override
  public boolean equals(Object object2) {
    if (!(object2 instanceof Circle) || !super.equals(object2)) return false;
    Circle shape2 = (Circle) object2;
    return shape2.radius == radius;
  }

}
