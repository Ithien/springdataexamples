package com.jdelgadom.springdataexamples.designpatterns.iterator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Transformer {
  String name;
  String team;
  String weapon;

  public Transformer(String name, String team, String weapon) {
    this.name = name;
    this.team = team;
    this.weapon = weapon;
  }

  @Override
  public String toString() {
    return "Transformer{" + "name='" + name + '\'' + ", team='" + team + '\'' + ", weapon='" + weapon + '\'' + '}';
  }
}
