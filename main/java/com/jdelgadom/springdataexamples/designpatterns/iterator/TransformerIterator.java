package com.jdelgadom.springdataexamples.designpatterns.iterator;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class TransformerIterator implements Iterator<Transformer> {

  private List<Transformer> transformerList;
  private int location = 0;

  public TransformerIterator(List<Transformer> transformerList) {
    this.transformerList = transformerList;
  }

  /**
   * Returns {@code true} if the iteration has more elements.
   * (In other words, returns {@code true} if {@link #next} would
   * return an element rather than throwing an exception.)
   *
   * @return {@code true} if the iteration has more elements
   */
  @Override
  public boolean hasNext() {
    boolean result = false;
    if (transformerList != null && location < transformerList.size() && transformerList.get(location) != null){
      result = true;
    }
    return result;
  }

  /**
   * Returns the next element in the iteration.
   *
   * @return the next element in the iteration
   * @throws NoSuchElementException if the iteration has no more elements
   */
  @Override
  public Transformer next() {
    return transformerList.get(location++);
  }
}
