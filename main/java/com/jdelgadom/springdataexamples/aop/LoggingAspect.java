package com.jdelgadom.springdataexamples.aop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @NameClass: LoggingAspect
 * @author: @jdelgadomolto
 * @Description: LoggingAspect example to all the application
 * @DateCreation: 17/11/2018
 */
@Component
@Aspect
public class LoggingAspect {

  private final Log log = LogFactory.getLog(getClass());

  @Around("execution (* com.jdelgadom..*.*(..) )")
  public Object log(ProceedingJoinPoint pjp) throws Throwable{

    //Before invocation
    //Transaction start
    log.info("before " + pjp.toString());
    Object object = pjp.proceed();
    log.info("after " + pjp.toString());
    //Close transaction
    //After invocation

    return object;
  }
}
